function round(value, exp) {
  if (typeof exp === 'undefined' || +exp === 0)
    return Math.round(value);

  value = +value;
  exp = +exp;

  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
    return NaN;

  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}


function calculo2015() {

// Define todas as variáveis
var maritalstatus = document.getElementById("maritalstatus").value;
var rendaspa = document.getElementById("rendaspa").valueAsNumber;
var rendaspb = document.getElementById("rendaspb").valueAsNumber;
var rendbspa = document.getElementById("rendbspa").valueAsNumber;
var rendbspb = document.getElementById("rendbspb").valueAsNumber;
var anoaberacta = document.getElementById("anoaberacta").valueAsNumber;
var anoaberactb = document.getElementById("anoaberactb").valueAsNumber;
var tiporendbspa = document.getElementById("tiporendbspa").valueAsNumber;
var tiporendbspb = document.getElementById("tiporendbspb").valueAsNumber;
var whtspa = document.getElementById("whtspa").valueAsNumber;
var whtspb = document.getElementById("whtspb").valueAsNumber;
var ssspa = document.getElementById("ssspa").valueAsNumber;
var ssspb = document.getElementById("ssspb").valueAsNumber;
var surspa = document.getElementById("surspa").valueAsNumber;
var surspb = document.getElementById("surspb").valueAsNumber;
var numedep = document.getElementById("numedep").valueAsNumber;
var numedeptres = document.getElementById("numedeptres").valueAsNumber;
var numeasc = document.getElementById("numeasc").valueAsNumber;
var dgfSPA = document.getElementById("dgfSPA").valueAsNumber;
var dgfSPB = document.getElementById("dgfSPB").valueAsNumber;
var saudeSPA = document.getElementById("saudeSPA").valueAsNumber;
var saudeSPB = document.getElementById("saudeSPB").valueAsNumber;
var saudedep = document.getElementById("saudedep").valueAsNumber;
var eduSPA = document.getElementById("eduSPA").valueAsNumber;
var eduSPB = document.getElementById("eduSPB").valueAsNumber;
var edudep = document.getElementById("edudep").valueAsNumber;
var habSPA = document.getElementById("habSPA").valueAsNumber;
var habSPB = document.getElementById("habSPB").valueAsNumber;
var habdep = document.getElementById("habdep").valueAsNumber;
var laresSPA = document.getElementById("laresSPA").valueAsNumber;
var laresSPB = document.getElementById("laresSPB").valueAsNumber;
var laresdep = document.getElementById("laresdep").valueAsNumber;
var autoSPA = document.getElementById("autoSPA").valueAsNumber;
var autoSPB = document.getElementById("autoSPB").valueAsNumber;
var autodep = document.getElementById("autodep").valueAsNumber;
var motoSPA = document.getElementById("motoSPA").valueAsNumber;
var motoSPB = document.getElementById("motoSPB").valueAsNumber;
var motodep = document.getElementById("motodep").valueAsNumber;
var restSPA = document.getElementById("restSPA").valueAsNumber;
var restSPB = document.getElementById("restSPB").valueAsNumber;
var restdep = document.getElementById("restdep").valueAsNumber;
var cabeSPA = document.getElementById("cabeSPA").valueAsNumber;
var cabeSPB = document.getElementById("cabeSPB").valueAsNumber;
var cabedep = document.getElementById("cabedep").valueAsNumber;
var limite = 0;
var taxa1 = 0;
var taxa2 = 0;
var abater1 = 0;
var abater2 = 0;
var taxaadicional = 0;
var dedudgfSPA = 0;
var dedudgfSPB = 0;
console.log("rendaspa", rendaspa);

var limitematrix = math.matrix([[0, 300, 625, 1000], [0, 350, 750, 1200], [0, 600, 1250, 2000]]);
var taxasmatrix = math.matrix([[7000, 20000, 40000, 80000, math.number(math.bignumber('1e100'))], [0.145, 0.285, 0.37, 0.45, 0.48], [0, 980, 2680, 5880, 8280]]);


// Cálculo de deduções específicas Cat. A
	var deducespaspa = Math.min(Math.max(4104, ssspa), rendaspa);
	var deducespaspb = Math.min(Math.max(4104, ssspb), rendaspb);
	var i = 0;
	var z = 0;


// Cálculo de deduções específicas Cat. B
	if (tiporendbspa === "CIRS 151") {
	  deducespbspa = rendbspa * 0.25;
	} else if (tiporendbspa === "Outros serviços") {
	  deducespbspa = rendbspa * 0.6;
	} else {
	  deducespbspa = rendbspa * 0.85;
	}

	if (anoaberactb === 2015) {
	  deducespbspa = deducespbspa * 2;
	} else if (deducespbspa - 2015 === 1) {
	  deducespbspa = deducespbspa / 0.75;
	}


	if (tiporendbspb === "CIRS 151") {
	  deducespbspb = rendbspb * 0.25;
	} else if (tiporendbspb === "Outros serviços") {
	  deducespbspb = rendbspb * 0.6;
	} else {
	  deducespbspb = rendbspb * 0.85;
	}

	if (anoaberactb === 2015) {
	  deducespbspb = deducespbspb * 2;
	} else if (deducespbspb - 2015 === 1) {
	  deducespbspb = deducespbspb / 0.75;
	}

	var rendcolectavel = rendaspa + rendaspb + rendbspa + rendbspb - deducespaspa - deducespaspb - deducespbspa - deducespbspb;



	console.log("rendaspa", rendaspa);
	console.log("rendaspb", rendaspb);
	console.log("rendbspa", rendbspa);
	console.log("rendbspb", rendbspb);
	console.log("deducespaspa", deducespaspa);
	console.log("deducespaspb", deducespaspb);
	console.log("deducespbspa", deducespbspa);
	console.log("deducespbspb", deducespbspb);
	console.log("rendcolectavel",rendcolectavel);

// Cálculo do split a considerar e colecta
	// checka se é casado ou não, se tem filhos ou nÃ£o
	if (maritalstatus == "Casado/União de Facto") {
	  document.getElementById("rendaspb").value === 0;
	  document.getElementById("rendaspb").display = 'block';
	  document.getElementById("rendbspb").value === 0;
	  document.getElementById("rendbspb").display = 'block';
	  document.getElementById("whtspb").value === 0;
	  document.getElementById("whtspb").display = 'block';
	  document.getElementById("ssspb").value === 0;
	  document.getElementById("ssspb").display = 'block';
	  document.getElementById("whtspb").value === 0;
	  document.getElementById("whtspb").display === 'block';
	  document.getElementById("anoaberactb").value === 0;
	  document.getElementById("anoaberactb").display === 'block';
	  
	  // Para colecta com dependentes
	  splitting1 = round(rendcolectavel / (2 + 0.3 * (numedep + numedeptres + numeasc)),2);
	  
	  // Para colecta sem dependentes
	  splitting2 = round(rendcolectavel / 2,2);
		console.log('splitting1',splitting1);
		console.log('splitting2',splitting2);
	  // Para colecta com dependentes
	  while(taxasmatrix.subset(math.index(0, i))<splitting1){
	  	taxa1=taxasmatrix.subset(math.index(1, i+1));
	  	abater1=taxasmatrix.subset(math.index(2, i+1));
	  	i=i+1;
	  }
	  	console.log("i",i);
	  	console.log("taxa1",taxa1);
	  	console.log("abater1",abater1);

	  // Para colecta sem dependentes
	  while(taxasmatrix.subset(math.index(0, z))<splitting2){
	  	taxa2=taxasmatrix.subset(math.index(1, z+1));
	  	abater2=taxasmatrix.subset(math.index(2, z+1));
	  	z=z+1;
	  }
	  	console.log("z",z);
	  	console.log("taxa2",taxa2);
	  	console.log("abater2",abater2);


	  // Para colecta com dependentes
	  colectacomdep = round(((splitting1 * taxa1).toFixed(2) - abater1)*2,2);
	  // Para colecta sem dependentes
	  colectasemdep = round(((splitting2 * taxa2).toFixed(2) - abater2)*2,2);

	  console.log("colectacomdep", colectacomdep);
	  console.log("colectasemdep", colectasemdep);
	} else {
	  document.getElementById("rendaspb").value === 0;
	  document.getElementById("rendaspb").display = 'none';
	  document.getElementById("rendbspb").value === 0;
	  document.getElementById("rendbspb").display = 'none';
	  document.getElementById("whtspb").value === 0;
	  document.getElementById("whtspb").display = 'none';
	  document.getElementById("ssspb").value === 0;
	  document.getElementById("ssspb").display = 'none';
	  document.getElementById("whtspb").value === 0;
	  document.getElementById("whtspb").display === 'none';
	  document.getElementById("anoaberactb").value === 0;
	  document.getElementById("anoaberactb").display === 'none';

      // Para colecta com dependentes
	  splitting1 = round(rendcolectavel / (1 + 0.3 * (numedep + numedeptres + numeasc)),2);
	  // Para colecta sem dependentes
	  splitting2 = rendcolectavel / 1;

      // Para colecta com dependentes
	  while(taxasmatrix.subset(math.index(0, i))<splitting1){
	  	taxa1=taxasmatrix.subset(math.index(1, i+1));
	  	abater1=taxasmatrix.subset(math.index(2,i+1));
	  	i=i+1;
	  }
	  	console.log("i", i);
	  	console.log("taxa1", taxa1);
	  	console.log("abater1", abater1);
	  // Para colecta sem dependentes
	  while(taxasmatrix.subset(math.index(0,z))<splitting2){
	  	taxa2=taxasmatrix.subset(math.index(1,z+1));
	  	abater2=taxasmatrix.subset(math.index(2,z+1));
	  	z=z+1;
	  }
	  	console.log("z", z);
	  	console.log("taxa2", taxa2);
	  	console.log("abater2", abater2);
	  // Para colecta com dependentes
	  colectacomdep = round(splitting1 * taxa1,2) - abater1;
	  // Para colecta sem dependentes
	  colectasemdep = round(splitting2 * taxa2,2) - abater2;

	}

// Cálculo de limite de diff de colectas
	if (maritalstatus == "Casado/União de Facto") {
		
		limite = limitematrix.subset(math.index(2,Math.min(3,numedep + numedeptres)));
	} else{
		if (numedep + numedeptres > 0 ) {
			limite = limitematrix.subset(math.index(1,Math.min(3,numedep + numedeptres)))
		} else{
			limite = limitematrix.subset(math.index(0,Math.min(3,numedep + numedeptres)))
		}
	}

	console.log("abs diff",math.abs(colectacomdep - colectasemdep));
	console.log("limite", limite);

// Escolher a colecta
	if (math.abs(colectacomdep - colectasemdep) > limite) {
		splitting = splitting2
	  	colecta = colectasemdep - limite;
	} else {
		splitting = splitting1
	  	colecta = colectacomdep;
	}

// Cálculo de sobretaxa e taxa adicional de solidariedade
	if (maritalstatus == "Casado/União de Facto") {
		sobretaxa = round(((splitting - 505*14)*0.035)*2-12.13*(numedep + numedeptres),2);
		if (splitting >= 80000 && splitting <= 250000) {
		taxaadicional = (splitting-80000)*0.025*2;
		} else if (splitting > 250000){
		taxaadicional = ((250000-80000)*0.025+(splitting-250000)*0.05)*2;

		} else{
			taxaadicional = 0;
		}
		
	} else{
		
		sobretaxa = round(((splitting - 505*14)*0.035)-12.13*(numedep + numedeptres),2);
		if (splitting >= 80000 && splitting <= 250000) {
		taxaadicional = (splitting-80000)*0.025;
			} else if (splitting > 250000){
		taxaadicional = (250000-80000)*0.025+(splitting-250000)*0.05;
		} else{
			taxaadicional = 0;
		}

	}

console.log('splitting', splitting);

// Export para HTML
	document.getElementById('colecta').value = colecta;
	document.getElementById('sobretaxa').value = sobretaxa;
	document.getElementById('taxaadicional').value = round(taxaadicional,2);

	impostototal = colecta + sobretaxa + round(taxaadicional,2);

// Cálculo de deduções

	//Despesas Gerais e familiares
	if (maritalstatus == "Casado/União de Facto") {
		dedudgfSPA = round(math.min(0.35 * dgfSPA, 250),2);
		dedudgfSPB = round(math.min(0.35 * dgfSPB, 250),2);
	}else if(numedep + numedeptres> 0){
		dedudgfSPA = round(math.min(0.45 * dgfSPA, 335),2);
		dedudgfSPB = 0;
	} else{
		dedudgfSPA = round(math.min(0.35 * dgfSPA, 250),2);
		dedudgfSPB = 0;
	}

	//Deduções pessoais
	if (numedep + numedeptres > 3) {
		
	} else{};
// VER DEPENDENTES MAIS E MENOS DE 3 ANOS


//Despesas admitidas pelo CIRS sujeitas ao limite global
var deduiva = round(math.min((autoSPA + autoSPB + autodep + motoSPA + motoSPB + motodep + restSPA + restSPB + restdep + cabeSPA + cabeSPB + cabedep)*0.15,250),2);
var dedusaude = round(math.min((saudeSPA + saudeSPB + saudedep)*0.15,1000),2);	//Despesas de saúde à taxa reduzida
var deduedu = round(math.min((eduSPA + eduSPB + edudep)*0.3, 800),2); //despesas de educação
var deduhab = round(math.min((habSPA + habSPA + habdep)*0.15,502),2); //despesas de rendaspb
}